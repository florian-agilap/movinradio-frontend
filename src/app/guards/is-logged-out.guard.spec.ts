import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { AccountService } from '../account.service';
import { BackendApiService } from '../backend-api/backend-api.service';

import { IsLoggedOutGuard } from './is-logged-out.guard';

describe('IsLoggedOutGuard', () => {
  let guard: IsLoggedOutGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AccountService, BackendApiService],
      imports: [HttpClientTestingModule],
    });
    guard = TestBed.inject(IsLoggedOutGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
