import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from 'src/app/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });
  error: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private accountService: AccountService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  /**
   * Perform login with form values
   */
  onSubmit() {
    this.doLogin(
      this.loginForm.controls.username.value as string,
      this.loginForm.controls.password.value as string
    );
  }

  /**
   * Perform a login with given values
   * @param username Username
   * @param password Password
   */
  doLogin(username: string, password: string) {
    this.accountService.login(username, password).subscribe({
      next: () => this.router.navigate(['/playlists']),
      error: () => (this.error = true),
    });
  }
}
