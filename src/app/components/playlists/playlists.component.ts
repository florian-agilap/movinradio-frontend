import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BackendApiService } from 'src/app/backend-api/backend-api.service';
import { Playlist } from 'src/app/models/playlist.model';
import { PlaylistEditingComponent } from '../playlist-editing/playlist-editing.component';

@Component({
  selector: 'app-playlists',
  templateUrl: './playlists.component.html',
  styleUrls: ['./playlists.component.scss'],
})
export class PlaylistsComponent implements OnInit {
  playlists: Playlist[] = [];

  constructor(
    private backendApi: BackendApiService,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.refreshPlaylistsList();
  }

  /**
   * Refreshes displayed list of playlists
   */
  refreshPlaylistsList() {
    this.backendApi
      .getPlaylists()
      .subscribe((playlists) => (this.playlists = playlists));
  }

  /**
   * Open a modal to create or edit a playlist
   * @param playlist Playlist to edit (optional)
   */
  openPlaylistEditionModal(playlist?: Playlist) {
    const modalRef = this.modalService.open(PlaylistEditingComponent, {
      backdrop: 'static',
      size: 'lg',
    });
    if (playlist) {
      modalRef.componentInstance.playlist = playlist;
    }
    modalRef.closed.subscribe(() => this.refreshPlaylistsList());
  }

  /**
   * Remove a playlist
   * @param playlist Playlist to remove
   */
  removePlaylist(playlist: Playlist) {
    this.backendApi
      .deletePlaylist(playlist.id as number)
      .subscribe(() => this.refreshPlaylistsList());
  }

  /**
   * Open PDF export of all playlists
   */
  openPlaylistsExport() {
    this.backendApi.getPlaylistsPDFExport().subscribe((blob) => {
      const url = window.URL.createObjectURL(blob);
      window.open(url);
    });
  }

  /**
   * Open PDF export of a given playlist
   * @param playlist Playlist to export
   */
  openPlaylistExport(playlist: Playlist) {
    this.backendApi
      .getPlaylistPDFExport(playlist.id as number)
      .subscribe((blob) => {
        const url = window.URL.createObjectURL(blob);
        window.open(url);
      });
  }
}
