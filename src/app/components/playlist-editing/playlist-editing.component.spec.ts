import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbActiveModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from 'src/app/account.service';
import { BackendApiService } from 'src/app/backend-api/backend-api.service';

import { PlaylistEditingComponent } from './playlist-editing.component';

describe('PlaylistEditingComponent', () => {
  let component: PlaylistEditingComponent;
  let fixture: ComponentFixture<PlaylistEditingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [AccountService, BackendApiService, NgbActiveModal],
      imports: [
        HttpClientTestingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
      ],
      declarations: [PlaylistEditingComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(PlaylistEditingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
