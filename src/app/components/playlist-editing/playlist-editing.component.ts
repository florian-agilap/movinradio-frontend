import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AccountService } from 'src/app/account.service';
import { BackendApiService } from 'src/app/backend-api/backend-api.service';
import { Playlist } from 'src/app/models/playlist.model';
import { Track } from 'src/app/models/track.model';

@Component({
  selector: 'app-playlist-editing',
  templateUrl: './playlist-editing.component.html',
  styleUrls: ['./playlist-editing.component.scss'],
})
export class PlaylistEditingComponent implements OnInit {
  @Input() name?: string;
  @Input() playlist?: Playlist;
  providers?: string[];
  providerTracks?: Track[];
  selectedTrack?: Track;
  editingForm = this.formBuilder.group({
    owner: { value: '', disabled: true },
    name: ['', Validators.required],
    tracks: [[] as Track[]],
  });

  constructor(
    public activeModal: NgbActiveModal,
    public accountService: AccountService,
    private formBuilder: FormBuilder,
    private backendApi: BackendApiService
  ) {}

  ngOnInit(): void {
    this.backendApi
      .getTrackProviders()
      .subscribe((providers) => (this.providers = providers));

    if (this.playlist == null) {
      this.editingForm.setValue({
        owner: this.accountService.getUser() as string,
        name: '',
        tracks: [],
      });
    } else {
      this.editingForm.setValue({
        owner: this.playlist.owner,
        name: this.playlist.name,
        tracks: [...this.playlist.tracks],
      });
    }
  }

  /**
   *
   * @returns True if the component is in creation mode
   */
  isCreating() {
    return this.playlist == null;
  }

  /**
   * Fetch tracks for selected provider on provider change
   * @param event Change event
   */
  onProviderChange(event: any) {
    this.providerTracks = undefined;
    this.selectedTrack = undefined;
    this.backendApi
      .getTracksFromProvider(event.target.value)
      .subscribe(
        (providerTracks) =>
          (this.providerTracks = providerTracks.sort((a, b) =>
            a.title.localeCompare(b.title)
          ))
      );
  }

  /**
   * Set selected track on track change
   * @param event Change event
   */
  onProviderTracksChange(event: any) {
    this.selectedTrack = event.target.value;
  }

  /**
   * Add selected track to playlist tracks
   */
  addTrack() {
    if (this.selectedTrack != null) {
      this.editingForm.controls.tracks.value?.push(this.selectedTrack as Track);
    }
  }

  /**
   * Remove track from playlist tracks
   * @param index Track index in the playlist array
   */
  removeTrack(index: number) {
    this.editingForm.controls.tracks.value?.splice(index, 1);
  }

  /**
   * Create or edit the playlist with form values
   */
  onSubmit() {
    const editedPlaylist: Playlist = {
      owner: this.editingForm.controls.owner.value as string,
      name: this.editingForm.controls.name.value as string,
      tracks: this.editingForm.controls.tracks.value as Track[],
    };
    if (this.isCreating()) {
      this.backendApi
        .createPlaylist(editedPlaylist)
        .subscribe(() => this.activeModal.close());
    } else {
      editedPlaylist.id = this.playlist?.id;
      this.backendApi
        .updatePlaylist(editedPlaylist)
        .subscribe(() => this.activeModal.close());
    }
  }
}
