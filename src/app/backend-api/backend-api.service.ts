import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Playlist } from '../models/playlist.model';
import { Track } from '../models/track.model';
import { AuthenticationResponse } from './responses/authentication.response';

@Injectable({
  providedIn: 'root',
})
export class BackendApiService {
  constructor(private http: HttpClient) {}

  private handleError(error: HttpErrorResponse) {
    console.error('An error occurred:', error.error);
    return throwError(
      () => new Error('Something bad happened; please try again later.')
    );
  }

  authenticate(username: string, password: string) {
    return this.http
      .post<AuthenticationResponse>(
        environment.apiBaseUrl + '/auth/authenticate',
        {
          username,
          password,
        }
      )
      .pipe(catchError(this.handleError));
  }

  getPlaylists() {
    return this.http
      .get<Playlist[]>(environment.apiBaseUrl + '/playlists')
      .pipe(catchError(this.handleError));
  }

  createPlaylist(playlist: Playlist) {
    return this.http
      .post<Playlist>(environment.apiBaseUrl + '/playlists', playlist)
      .pipe(catchError(this.handleError));
  }

  updatePlaylist(playlist: Playlist) {
    return this.http
      .put<Playlist>(
        environment.apiBaseUrl + '/playlists/' + playlist.id,
        playlist
      )
      .pipe(catchError(this.handleError));
  }

  deletePlaylist(id: number) {
    return this.http
      .delete<{ deleted: boolean }>(environment.apiBaseUrl + '/playlists/' + id)
      .pipe(catchError(this.handleError));
  }

  getPlaylistsPDFExport() {
    return this.http
      .get(environment.apiBaseUrl + '/playlists/export', {
        headers: new HttpHeaders().set('Accept', 'application/pdf'),
        responseType: 'blob',
      })
      .pipe(catchError(this.handleError));
  }

  getPlaylistPDFExport(id: number) {
    return this.http
      .get(environment.apiBaseUrl + '/playlists/' + id + '/export', {
        headers: new HttpHeaders().set('Accept', 'application/pdf'),
        responseType: 'blob',
      })
      .pipe(catchError(this.handleError));
  }

  getTrackProviders() {
    return this.http
      .get<string[]>(environment.apiBaseUrl + '/tracks/providers')
      .pipe(catchError(this.handleError));
  }

  getTracksFromProvider(provider: string) {
    return this.http
      .get<Track[]>(environment.apiBaseUrl + '/tracks/' + provider)
      .pipe(catchError(this.handleError));
  }
}
