export interface Track {
  id: number;
  provider: string;
  trackProviderRef: string;
  title: string;
  artist: string;
  album: string;
}
