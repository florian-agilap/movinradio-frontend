import { Track } from './track.model';

export interface Playlist {
  id?: number;
  owner: string;
  name: string;
  tracks: Track[];
}
