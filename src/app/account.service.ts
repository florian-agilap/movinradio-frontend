import { Injectable } from '@angular/core';
import { EMPTY, mergeMap, Observable, of, shareReplay } from 'rxjs';
import { BackendApiService } from './backend-api/backend-api.service';
import jwt_decode from 'jwt-decode';
import { Router } from '@angular/router';

/**
 * There is no security check of the JWT here, in a real project, this should be configured better
 */
@Injectable({
  providedIn: 'root',
})
export class AccountService {
  private jwt: string | null = null;
  private user: string | null = null;

  constructor(private backendApi: BackendApiService, private router: Router) {
    this.jwt = localStorage.getItem('jwt');
    this.user = localStorage.getItem('user');
  }

  /**
   * Save JWT token in local storage
   * @param jwt JWT token
   */
  private setJwt(jwt: string | null) {
    this.jwt = jwt;
    if (jwt == null) {
      localStorage.removeItem('jwt');
    } else {
      localStorage.setItem('jwt', jwt as string);
    }
  }

  /**
   * Save username in local storage
   * @param user Username
   */
  private setUser(user: string | null) {
    this.user = user;
    if (user == null) {
      localStorage.removeItem('user');
    } else {
      localStorage.setItem('user', user as string);
    }
  }

  /**
   * Perform a login and save received JWT
   * @param username Username
   * @param password Password
   * @returns Observable on the logged in username
   */
  login(username: string, password: string) {
    return this.backendApi.authenticate(username, password).pipe(
      shareReplay(),
      mergeMap((authenticateResponse) => {
        this.setJwt(authenticateResponse.accessToken);
        this.setUser((jwt_decode(this.jwt as string) as any).sub);
        return of(this.getUser());
      })
    );
  }

  /**
   * Perform a logout
   */
  logout() {
    this.setJwt(null);
    this.setUser(null);
    this.router.navigate(['/login']);
  }

  /**
   *
   * @returns True if user is logged in
   */
  isLoggedIn() {
    return this.jwt != null;
  }

  /**
   *
   * @returns Current JWT token if exists, null otherwise
   */
  getJwt() {
    return this.jwt;
  }

  /**
   *
   * @returns Current username if exists, null otherwise
   */
  getUser() {
    return this.user;
  }
}
