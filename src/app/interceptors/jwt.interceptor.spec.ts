import { TestBed } from '@angular/core/testing';
import { AccountService } from '../account.service';
import { BackendApiService } from '../backend-api/backend-api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { JwtInterceptor } from './jwt.interceptor';

describe('JwtInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [JwtInterceptor, AccountService, BackendApiService],
      imports: [HttpClientTestingModule],
    })
  );

  it('should be created', () => {
    const interceptor: JwtInterceptor = TestBed.inject(JwtInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
