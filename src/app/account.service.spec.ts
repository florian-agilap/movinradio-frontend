import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { environment } from 'src/environments/environment';

import { AccountService } from './account.service';
import { BackendApiService } from './backend-api/backend-api.service';

describe('AccountService', () => {
  let httpTestingController: HttpTestingController;
  let service: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BackendApiService],
      imports: [HttpClientTestingModule],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AccountService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('call to login should send request to authenticate', () => {
    const testUsername = 'testUsername';
    const testPassword = 'testPassword';

    service.login(testUsername, testPassword).subscribe();

    const req = httpTestingController.expectOne(
      environment.apiBaseUrl + '/auth/authenticate'
    );
    expect(req.request.method).toEqual('POST');
    expect(req.request.body.username).toEqual(testUsername);
    expect(req.request.body.password).toEqual(testPassword);
  });

  it('call to login should store JWT in local storage', () => {
    const testToken =
      'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyMSIsIkF1dGhvcml0aWVzIjpbXSwiZXhwIjoxNjY3MzYyMzA5LCJpYXQiOjE2NjczNTg3MDl9.wOtSxdG8MafbZwyrGehrm3hKfqR1f--5ECI_1cRFJC2O6KwTYMWDnaE2JG9n4kZ-BUtukuysaWBDAGQqIXoE5w';

    service.login('testUsername', 'testPassword').subscribe();

    const req = httpTestingController.expectOne(
      environment.apiBaseUrl + '/auth/authenticate'
    );
    req.flush({
      accessToken: testToken,
    });
    expect(localStorage.getItem('jwt')).toEqual(testToken);
  });
});
