# Movinradio Frontend

## Prerequisites

Have [NodeJS 16+ with NPM](https://nodejs.org/en/) installed

## Development

Install dependencies with:

```bash
npm install
```

Run the project with:

```bash
npm run start
```

Run tests with:

```bash
npm run test
```
